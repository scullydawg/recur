CC=g++
CFLAGS=-std=c++17 -g -Wall


all:
	$(CC) $(CFLAGS) *.cpp -o recursion.out


clean:
	rm -f *.o *.out


run:
	./recursion.out

run-debug:
	gdb ./recursion.out
